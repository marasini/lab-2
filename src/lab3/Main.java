package lab3;

public class Main {

	public static void main(String[] args) {
		
        employee employee1 = new Developer("name1", "surname1", 100);
		employee employee2 = new Developer("name2", "surname2", 200);
		employee employee3 = new Developer("name3", "surname3", 300);



		employee manager = new Manager("pam", "Donals", 3000,100 );
		employee manager1 = new Manager("Ram", "Dahal", 3500, 100);
		
		employee1.setAddress("country1", "city1", "street1", 100);
		employee2.setAddress("country2", "city2", "street2", 200);
		employee3.setAddress("country3", "city3", "street3", 300);

		manager.setAddress("Poland", "Wroclaw", "Krucha", 84);
		manager1.setAddress("Nepal", "KTM", "Kuleshwor", 98);
		

		((Manager) manager).addSubordinates((employee)employee1 );
		((Manager) manager1).addSubordinates((employee) employee2);
		
		System.out.println(employee1.toString());
		System.out.println(employee2.toString());
		System.out.println(employee3.toString());
		
		
		System.out.println(manager.toString());
		System.out.println(manager1.toString());
		


	}


}