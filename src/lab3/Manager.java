package lab3;

import java.util.ArrayList;
import java.util.List;

public class Manager extends employee {
	private int bonus;
	private List<employee>subordinates = new
			ArrayList<employee>();
			
	 //constructor 
	public Manager (String name, String surname,int salary,int bonus) {
		super (name,surname,salary);
		this.bonus=bonus;
	}
	//subordinates getter
	public List<employee>getSubordination() {
		return subordinates ;
	}
		
		//add employee as a subordinates
		public void addSubordinates (employee Employee) {
			subordinates.add(Employee);
		}
	
	//calculate salary
	public int getsalary() {
		return super.getSalary() + this.bonus;
	}
		 
		
		//print
		public String toString () {
			String subordinatesList = "" ;
			if (subordinates != null) {
				for (employee Employee : subordinates) {
					subordinatesList += "Subordinate"+ (Employee).toString();
				}
			}
			return super.toString() + subordinatesList;
			
		
			
		}
		
}


		
	
