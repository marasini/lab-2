package lab3;

public abstract class employee {
	private String name;
	private String surname;
	private String Country,city,street;
	int streetNum;
	private int salary;

	//constructor
public employee(String name,String surname,int salary) {
	
	this.name=name;
	this.surname=surname;
	this.salary= salary;
}

//SetAddress
public void setAddress (String country,String city,String street,int streetNum) {
this.Country =country;
this.city=city;
this.street=street;
this.streetNum= streetNum;

}

//calculate salary
public int getSalary() {
	return this.salary;
}

//print the information
public String toString() {
	return getClass().getSimpleName() + "-" + name + " " + surname + " " + Country +" ," + city + "," +street+"," +streetNum+ ",salary:" +
getSalary() + "\r\n";
	
	
}
}